﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControls : MonoBehaviour {

    private Vector3 mousePosition;
    private Rigidbody2D rb;
    private Vector2 direction;
    [SerializeField]
    private float moveSpeed = 100f;

    [SerializeField]
    private float dashPower = 100f;
    [SerializeField]
    private float dashTime = 1f;
    private bool isDashing = false;
    private float currentDashTime;
    //[SerializeField]
    //private float dashCooldown = 1f;
    [SerializeField]
    private float maxDistance = 100f;
    private Vector3 dashMoveVector;
    private Vector3 travelDistance;
    private float currentDistance;
    private Vector2 startPos;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    void FixedUpdate () {

        if (isDashing == true)
        {
            currentDistance = Vector2.Distance(startPos, transform.position);
            if (currentDistance >= maxDistance)
            {
                isDashing = false;
            }
        }

        if (isDashing == false)
        {
            //the 3 lines below are to make the player follow the mouse\\
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = (mousePosition - transform.position).normalized;
            rb.velocity = new Vector2(direction.x * moveSpeed, direction.y * moveSpeed);
        }

        if (Input.GetMouseButtonDown(0))
        {
            currentDashTime = dashTime;

            rb.velocity = new Vector2(0f, 0f);

            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 startPos = transform.position;

            direction = (mousePosition - transform.position).normalized;
            travelDistance = new Vector3(direction.x * dashPower, direction.y * dashPower);
            dashMoveVector = Vector3.ClampMagnitude(travelDistance, maxDistance);
            rb.velocity = dashMoveVector;
        }

        if (currentDashTime > 0)
        {
            isDashing = true;
            currentDashTime -= Time.deltaTime;
        }

        else
        {
            isDashing = false;
        }
	}
}
